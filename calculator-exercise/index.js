var app = new Vue({
  el: '#calculator',
  data: {
    arithString: "0",
    operation: true,
    num: false
  },
  computed: {
    isOperationsDisabled () {
    	return this.operation;
    },
    isNumbersDisabled() {
    	return this.num;
    }
  },
  methods: {
  	clicked: function (event){
		let key = event.target.innerText;
		if(key === "=") {
			equals();
		}else if(key === "c"){
			app.arithString = "0";
			    this.operation= true;
    			this.num = false;
		}else{
			if(this.arithString === "0" 
					&& !isArithOp(key)){
				this.arithString = key;	
				this.operation = false;
			}else{
				if(isArithOp(key)){
					if(this.num === true)
						this.num = false;	
					this.operation = true;
				}
				this.arithString += key;
			}
		}
	}
  }
});

function equals() {
	app.arithString = eval(app.arithString);
	app.operation = false;
	app.num = true;
}

function isArithOp(key) {
	return key === "+" || key === "/" || key === "-" || key === "*"
}