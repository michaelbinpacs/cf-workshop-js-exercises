// implement simple todo list using VueJs :)
new Vue({
	el: '#todo-list',
	data: {
		input: '',
		itemlist: [],
		errorPresence: false
	},
	methods: {
		// a computed getter
		addlist: function() {			
			this.errorPresence = !((this.input !== '') ? this.itemlist.push({ id: this.itemlist.length, item: this.input }) : false);
			this.input = '';
		},
		deleteItem: function(event) {
			console.log(event.target.id);
			this.itemlist.splice(event.target.id, 1);
			for (let i = 0; i < this.itemlist.length; i++) {
				(this.itemlist[i] !== undefined) 
					? this.itemlist[i].id = i
					: console.log("Empty");
			}
		}
	}
})